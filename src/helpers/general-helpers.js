const env = require('../env');

module.exports = {
    disableInfoLogsIfNeeded: function(){
        if (!env.printInfoMessages) {
            console.info = function () {
            }
        }
    },
    requestValidator: function (req) {
        if (!(req.body && req.body.message && req.body.time)) {
            return {"message": "You must send valid json with message and time field."}
        };

        let timestamp = Date.parse(req.body.time);

        if (isNaN(timestamp) === true) {
            return {"message": "body.time: You must send valid date in ISO 8601 format."}
        };

        let timeToDelay = Math.floor(((new Date(timestamp)) - Date.now()) / 1000);

        if (timeToDelay < 0) {
            return {"message": "body.time: You must send valid date in the future."}
        }

    }
};