const RedisSMQ = require('rsmq');
const env = require('../env');

let eventQueue = env.eventQueue;

module.exports = {
    initializeConsumer: function (rsmq) {
        // listen to new messages on a loop
        setInterval(function () {
            rsmq.receiveMessage({qname: eventQueue}, function (err, resp) {
                    let messageId = resp.id;
                    if (messageId) {
                        console.log(resp.message);
                        rsmq.deleteMessage({qname: eventQueue, id: messageId}, function (err, resp) {
                            if (resp === 1) {
                                console.info(`Message with id:${messageId} was deleted.`)
                            }
                            else {
                                console.error(`Message with id:${messageId} not found.`)
                            }
                        });
                    }
                }
            )
        }, env.intervalInMs);
    },
    initializeQueue: function (rsmq) {
        rsmq.createQueue({qname: eventQueue}, function (err, resp) {
            if (resp === 1) {
                console.log(`queue ${eventQueue} is been created`)
            }
        });
    },
    initializeRsmq: function () {
        let rsmq = new RedisSMQ({host: env.redisHost, port: env.redisPort, ns: env.redisNameSpace})
            .on("disconnect", function () {
                console.log("There was an error connecting to redis, exiting")
                process.exit(1)
            });
        return rsmq;
    }
};