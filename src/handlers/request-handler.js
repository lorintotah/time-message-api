const env = require('../env');
const helpers = require('../helpers/general-helpers');

module.exports = {
    postEchoAtTime: async function (req, res, rsmq) {
        // validations
        let err = helpers.requestValidator(req, res);
        if (err) {
            res.status(400).send(err);
            return;
        }
        let {message,time} = req.body;

        let timeToDelay = Math.floor(((new Date(time)) - Date.now()) / 1000);
        console.info(`The message will be print in ${timeToDelay} seconds.`);
        rsmq.sendMessage({qname: env.eventQueue, message: message, delay: timeToDelay}, function (err, resp) {
            if (resp) {
                console.info("Message sent. ID:", resp);
            }
        });

        res.status(204).end();

    }
}