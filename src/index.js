const express = require('express');
const app = express();
const env = require('./env');
let bodyParser = require('body-parser');
let requestHandler = require('./handlers/request-handler');
let helpers = require('./helpers/general-helpers');
let queueHandler = require('./handlers/queue-handler');

// parse body as json
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({extended: true})); // support encoded bodies

// initialize the queue
let rsmq = queueHandler.initializeRsmq();

helpers.disableInfoLogsIfNeeded();
queueHandler.initializeQueue(rsmq);
queueHandler.initializeConsumer(rsmq);

// handle api post request
app.post('/echoAtTime', (req, res) => requestHandler.postEchoAtTime(req, res, rsmq));

// start express server
app.listen(env.port, () => console.log(`Time message queue running on port:${env.port}`));
