module.exports = {
    port: process.env.port || '3000',
    redisHost: process.env.redisHost || "127.0.0.1",
    redisPort: parseInt(process.env.redisPort) || 6379,
    redisNameSpace: process.env.redisNameSpace || "exam",
    eventQueue: process.env.eventQueue || "eventQueue",
    printInfoMessages: process.env.printInfoMessages === "true",
    intervalInMs: parseInt(process.env.intervalInMs) || 1000,
};